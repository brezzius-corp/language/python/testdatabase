#!/usr/bin/python

import pyodbc 

try:
    f = open("database.txt")
except FileNotFoundError:
    print("Le fichier database.txt n'existe pas")
    exit(1)

l = f.readlines()

hostname = ""
database = ""
username = ""
password = ""
schema = ""

for elt in l:
    ele = elt.split('=')

    if ele[0] == "hostname":
        hostname = ele[1][:-1]
    elif ele[0] == "database":
        database = ele[1][:-1]
    elif ele[0] == "username":
        username = ele[1][:-1]
    elif ele[0] == "password":
        password = ele[1][:-1]
    elif ele[0] == "schema":
        schema = ele[1][:-1]

print("Connexion à la base de données...\n")

try:
    cnx = pyodbc.connect("DRIVER={SQL Server};SERVER=" + hostname + ";DATABASE=" + database + ";UID=" + username + ";PWD=" + password)
except pyodbc.InterfaceError:
    print("La connexion à la base de données à échoué")
    exit(0)
except pyodbc.ProgrammingError:
    print("La connexion à la base de données à échoué")
    exit(0)
except pyodbc.OperationalError:
    print("La connexion à la base de données à échoué")
    exit(0)

print("La connexion à la base de données a réussie")

cnx.close()

